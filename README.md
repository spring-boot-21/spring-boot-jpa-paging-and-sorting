## Pre-requisitos
- Spring Boot 2.3.4.RELEASE
- Spring 5.2.9.RELEASE
- Spring Data 2.4.1
- Thymeleaf 3.0.11.RELEASE
- Tomcat embed 9.0.38
- Gradle 6.7
- Java 11
- MySQL 8

## Construir proyecto
```
gradle build
```

## Ejecutar proyecto
```
gradle run
```

Accede a la página utilizando las URL:
- http://localhost:8080/springboot-jpa-paging-and-sorting/pagina/1/idCiudad/asc

Demo:
- https://zademy.com/springboot-jpa-paging-and-sorting/pagina/1/idCiudad/asc

