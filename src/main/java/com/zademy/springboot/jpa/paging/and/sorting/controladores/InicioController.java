package com.zademy.springboot.jpa.paging.and.sorting.controladores;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

import com.zademy.springboot.jpa.paging.and.sorting.entidades.Ciudad;
import com.zademy.springboot.jpa.paging.and.sorting.servicios.CiudadService;

/**
 * The Class InicioController.
 */
@Controller
public class InicioController {

	/** The Constant logger. */
	private static final Logger logger = LoggerFactory.getLogger(InicioController.class);

	/** The ciudad service. */
	@Autowired
	@Qualifier("ciudadService")
	private CiudadService ciudadService;

	/**
	 * Ver ciudades.
	 *
	 * @param numeroPagina the numero pagina
	 * @param campoOrden   the campo orden
	 * @param sentidoOrden the sentido orden
	 * @return the model and view
	 */
	@GetMapping("/pagina/{numeroPagina}/{campoOrden}/{sentidoOrden}")
	public ModelAndView verCiudades(@PathVariable("numeroPagina") Integer numeroPagina,
			@PathVariable("campoOrden") String campoOrden, @PathVariable("sentidoOrden") String sentidoOrden) {

		ModelAndView mav = new ModelAndView("index");

		/*
		 * Total de elementos por pagina
		 * */
		int tamanioPagina = 15;

		Page<Ciudad> page = ciudadService.obtenerCiudades(numeroPagina, tamanioPagina, campoOrden, sentidoOrden);

		List<Ciudad> listaCiudad = page.getContent();

		mav.addObject("numeroPagina", numeroPagina);
		mav.addObject("totalPaginas", page.getTotalPages());
		mav.addObject("totalElementos", page.getTotalElements());
		mav.addObject("listaCiudad", listaCiudad);

		mav.addObject("campoOrden", campoOrden);
		mav.addObject("sentidoOrden", sentidoOrden);
		mav.addObject("tipoSentidoOrden", sentidoOrden.equals("asc") ? "desc" : "asc");

		logger.info("Muestra vista index.html con resutados");

		return mav;

	}

}
