package com.zademy.springboot.jpa.paging.and.sorting.entidades;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The Class Ciudad.
 */
@Entity
@Table(name = "city")
public class Ciudad implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8166702833824607499L;

	/** The id ciudad. */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Integer idCiudad;

	/** The nombre. */
	@Column(name = "Name")
	private String nombre;

	/** The codigo pais. */
	@Column(name = "CountryCode")
	private String codigoPais;

	/** The distrito. */
	@Column(name = "District")
	private String distrito;

	/** The poblacion. */
	@Column(name = "Population")
	private Integer poblacion;

	/**
	 * Gets the id ciudad.
	 *
	 * @return the id ciudad
	 */
	public Integer getIdCiudad() {
		return idCiudad;
	}

	/**
	 * Sets the id ciudad.
	 *
	 * @param idCiudad the new id ciudad
	 */
	public void setIdCiudad(Integer idCiudad) {
		this.idCiudad = idCiudad;
	}

	/**
	 * Gets the nombre.
	 *
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Sets the nombre.
	 *
	 * @param nombre the new nombre
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * Gets the codigo pais.
	 *
	 * @return the codigo pais
	 */
	public String getCodigoPais() {
		return codigoPais;
	}

	/**
	 * Sets the codigo pais.
	 *
	 * @param codigoPais the new codigo pais
	 */
	public void setCodigoPais(String codigoPais) {
		this.codigoPais = codigoPais;
	}

	/**
	 * Gets the distrito.
	 *
	 * @return the distrito
	 */
	public String getDistrito() {
		return distrito;
	}

	/**
	 * Sets the distrito.
	 *
	 * @param distrito the new distrito
	 */
	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}

	/**
	 * Gets the poblacion.
	 *
	 * @return the poblacion
	 */
	public Integer getPoblacion() {
		return poblacion;
	}

	/**
	 * Sets the poblacion.
	 *
	 * @param poblacion the new poblacion
	 */
	public void setPoblacion(Integer poblacion) {
		this.poblacion = poblacion;
	}

	/**
	 * Instantiates a new ciudad.
	 */
	public Ciudad() {
		super();
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Ciudad [idCiudad=");
		builder.append(idCiudad);
		builder.append(", nombre=");
		builder.append(nombre);
		builder.append(", codigoPais=");
		builder.append(codigoPais);
		builder.append(", distrito=");
		builder.append(distrito);
		builder.append(", poblacion=");
		builder.append(poblacion);
		builder.append("]");
		return builder.toString();
	}

}
