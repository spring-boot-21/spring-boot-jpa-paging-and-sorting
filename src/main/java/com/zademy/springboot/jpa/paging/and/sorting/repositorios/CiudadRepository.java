package com.zademy.springboot.jpa.paging.and.sorting.repositorios;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.zademy.springboot.jpa.paging.and.sorting.entidades.Ciudad;

/**
 * The Interface CiudadRepository.
 */
@Repository("ciudadRepository")
public interface CiudadRepository extends JpaRepository<Ciudad, Integer> {

}
