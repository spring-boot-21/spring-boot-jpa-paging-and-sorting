package com.zademy.springboot.jpa.paging.and.sorting.servicios;

import org.springframework.data.domain.Page;

import com.zademy.springboot.jpa.paging.and.sorting.entidades.Ciudad;

/**
 * The Interface CiudadService.
 */
public interface CiudadService {

	/**
	 * Obtener ciudades.
	 *
	 * @param numeroPagina  the numero pagina
	 * @param tamanioPagina the tamanio pagina
	 * @param campoOrden    the campo orden
	 * @param sentidoOrden  the sentido orden
	 * @return the page
	 */
	Page<Ciudad> obtenerCiudades(int numeroPagina, int tamanioPagina, String campoOrden, String sentidoOrden);

}
