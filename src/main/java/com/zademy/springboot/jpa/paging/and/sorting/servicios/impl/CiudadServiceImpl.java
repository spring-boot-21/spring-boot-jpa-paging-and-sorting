package com.zademy.springboot.jpa.paging.and.sorting.servicios.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.zademy.springboot.jpa.paging.and.sorting.entidades.Ciudad;
import com.zademy.springboot.jpa.paging.and.sorting.repositorios.CiudadRepository;
import com.zademy.springboot.jpa.paging.and.sorting.servicios.CiudadService;

/**
 * The Class CiudadServiceImpl.
 */
@Service("ciudadService")
public class CiudadServiceImpl implements CiudadService {

	/** The ciudad repository. */
	@Autowired
	private CiudadRepository ciudadRepository;

	/**
	 * Obtener ciudades.
	 *
	 * @param numeroPagina  the numero pagina
	 * @param tamanioPagina the tamanio pagina
	 * @param campoOrden    the campo orden
	 * @param sentidoOrden  the sentido orden
	 * @return the page
	 */
	@Override
	public Page<Ciudad> obtenerCiudades(int numeroPagina, int tamanioPagina, String campoOrden, String sentidoOrden) {

		Pageable pageable = PageRequest.of(numeroPagina - 1, tamanioPagina,
				sentidoOrden.equals("asc") ? Sort.by(campoOrden).ascending() : Sort.by(campoOrden).descending());

		return ciudadRepository.findAll(pageable);
	}

}
